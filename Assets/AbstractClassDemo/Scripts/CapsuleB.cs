using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class CapsuleB : AbstractCapsule
{

    public Material defaultMaterial;
    public Material efectMaterial;

    protected async override void TargetChangeBehaviour()
    {
        Debug.Log("TargetChangeBehaviour");

        GetComponent<MeshRenderer>().material = efectMaterial;
        await System.Threading.Tasks.Task.Delay(70);
        GetComponent<MeshRenderer>().material = defaultMaterial;
    }

    protected override void SpesicifBehaviour()
    {
        TargetChangeBehaviour();
    }

}
