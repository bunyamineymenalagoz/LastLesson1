using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public abstract class AbstractCapsule : MonoBehaviour
{

    Vector3 currentTarget = new Vector3(0F, 1.44F, 0F);

    private void Awake()
    {

    }

    private void Start()
    {
    }

    private void Update()
    {
        if (Vector3.Distance(transform.position, currentTarget) < 1f)
        {
            currentTarget = new Vector3(UnityEngine.Random.Range(-5f, 5f), 1.44f, UnityEngine.Random.Range(-5f, 5f));
            SpesicifBehaviour();
        }
    }

    protected virtual void TargetChangeBehaviour()
    {
        Debug.Log("TargetChangeBehaviour");
    }

    protected virtual void FixedUpdate()
    {
        Motion();
    }

    protected void Motion()
    {
        Vector3 currentPosition = new Vector3(transform.position.x, 1.44f, transform.position.z);
        Vector3 direction = currentTarget - currentPosition;
        transform.position += direction.normalized * Time.deltaTime * 2.5f;
    }

    protected abstract void SpesicifBehaviour();

}
