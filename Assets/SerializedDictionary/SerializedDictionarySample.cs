using RotaryHeart.Lib.SerializableDictionary;

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class SerializedDictionarySample : MonoBehaviour
{

    public StudentInfos StudentInfos;

}



[System.Serializable]
public class StudentInfos : SerializableDictionaryBase<int, Student> { }




[System.Serializable]
public struct Student
{
    public string Name;
    public string SurName;
    public string Address;
}