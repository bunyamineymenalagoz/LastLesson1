using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class DemoManager : MonoBehaviour
{

    public UnityGameEngine UnityGameEngine;
    public UnrealEngine UnrealEngine;

    private IGameEngine currentGameEngine;


    private void Awake()
    {
        float val = UnityEngine.Random.value;
        currentGameEngine = UnityGameEngine;


    }

    private void Start()
    {
        int number1;
        int number2 = 6;

        currentGameEngine.CreatingGame(out number1, ref number2);
    }

}
