using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

public class DelegateSample5 : MonoBehaviour
{

    private void Start()
    {
        TimeOffsetStruct.SetUpTimeOffset(3f, () =>
        {

            Debug.Log("SetUpTimeOffset");

        });
    }


}
