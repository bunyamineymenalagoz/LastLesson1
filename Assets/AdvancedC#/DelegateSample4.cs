using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

public class DelegateSample4 : MonoBehaviour
{

    public delegate void NumberDelegate(int number);
    public delegate void NormalDelegate();

    public NumberDelegate delegateObject;
    public NormalDelegate delegateObject2;

    private void Start()
    {

        delegateObject += (int number) =>
       {
           Debug.Log(number);
       };


        delegateObject2 += delegate
         {
             Debug.Log("This is log");
         };




        delegateObject(5);
        delegateObject2();

    }

}
