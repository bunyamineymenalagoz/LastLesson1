using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

public class DelegateUnityEvent : MonoBehaviour
{

    public UnityEvent UnityEvent1;

    private void Start()
    {
        UnityEvent1.Invoke();

        //gameObject.BroadcastMessage("BroadcastMessageMethod", 5);
    }


    //public void BroadcastMessageMethod(int number)
    //{
    //    Debug.Log($"BroadcastMessageMethod {number}");
    //}

}
