using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class DelegateSample1 : MonoBehaviour
{

    public delegate void NumberDelegate(int number);

    public NumberDelegate delegateObject;

    private void OnEnable()
    {
        //delegateObject = NumberTest1;
        //delegateObject = NumberTest2;
        //delegateObject = NumberTest3;
        //delegateObject = NumberTest4;
        //delegateObject = NumberTest5;
    }

    private void OnDisable()
    {
        delegateObject = null;
    }

    private async void Start()
    {
        await System.Threading.Tasks.Task.Delay(3000);


        delegateObject.Invoke(4);
        await System.Threading.Tasks.Task.Delay(1000);
        delegateObject(5);

    }

    private void NumberTest1(int number1)
    {
        Debug.Log("test time" + number1);
    }

    private static void NumberTest2(int number)
    {
        number *= 2;
        Debug.Log(number);
    }

    private bool NumberTest3(int number)
    {
        return false;
    }

    private void NumberTest4(float number)
    {
        Debug.Log("test time" + number);
    }

    private void NumberTest5(int number1, int number2)
    {
        Debug.Log("test time" + number1 + " " + number2);
    }

}
