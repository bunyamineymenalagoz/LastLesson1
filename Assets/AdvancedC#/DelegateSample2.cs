using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class DelegateSample2 : MonoBehaviour
{

    public delegate int NumberDelegate2(string text);

    public NumberDelegate2 delegateObject;


    private void Awake()
    {
        delegateObject += NumberTest1;
        delegateObject += NumberTest2;

        if (delegateObject != null)
        {
            System.Delegate[] methods = delegateObject.GetInvocationList();

            for (int i = 0; i < methods.Length; i++)
            {
                int result = ((NumberDelegate2)methods[i]).Invoke("Test Delegate Loop");
                Debug.Log($"result {result}");
            }
        }


    }

    private int NumberTest1(string text)
    {
        return text.Length;
    }

    private static int NumberTest2(string text)
    {
        return -1;
    }

}
