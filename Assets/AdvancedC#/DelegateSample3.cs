using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

public class DelegateSample3 : MonoBehaviour
{

    #region Definitions

    public delegate void NumberDelegate(int number);

    [System.Serializable]
    public class NumberUnityEvent : UnityEvent<int> { }

    #endregion

    #region Delegate Objects

    public NumberDelegate delegateObject;

    public NumberUnityEvent unityEventObject;

    #endregion


    private void Start()
    {
        delegateObject += NumberTest1;
        unityEventObject.AddListener(NumberTest1);

        if (delegateObject != null)
            delegateObject(5);

        if (delegateObject != null)
            unityEventObject.Invoke(6);

        // remove NumberTest1

        delegateObject -= NumberTest1;
        unityEventObject.RemoveListener(NumberTest1);


        // remove all

        delegateObject = null;
        unityEventObject.RemoveAllListeners();

    }

    private void NumberTest1(int number)
    {
        Debug.Log(number);
    }
}
