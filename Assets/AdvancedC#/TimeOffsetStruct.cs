using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class TimeOffsetStruct : MonoBehaviour
{

    private static TimeOffsetStruct instance;

    public static void SetUpTimeOffset(float second, System.Action method)
    {
        if (instance == null)
        {
            instance = new GameObject().AddComponent<TimeOffsetStruct>();

            DontDestroyOnLoad(instance.gameObject);
        }

        if (method != null)
        {
            instance.StartCoroutine(RunMethodWithTimeoffset(second, method));
        }
    }

    private static IEnumerator RunMethodWithTimeoffset(float second, System.Action method)
    {
        yield return new WaitForSeconds(second);
        method();
    }

}
